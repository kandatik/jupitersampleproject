package test.automation.core;

public class Property {
	
	public static String getProp(String key) {
		return Props.getProp(key);
	}

	public static String getSystemProp(String key) {
		return Props.getSystemProp(key);
	}

	public static String setSystemProp(String key, String val) {
		return Props.setSystemProp(key, val);
	}

	public static void loadRunConfigProps(String configPropertyFileLocation) {
		Props.loadRunConfigProps(configPropertyFileLocation);
	}

	public static String getBrowserName() {
		return Props.getBrowserName();
	}
}
