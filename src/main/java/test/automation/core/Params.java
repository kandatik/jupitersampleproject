package test.automation.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Params {
	public static String ENV;
	public static boolean STEP_RECORD;

	protected static String DRIVER_PATH;
	public static String DRIVER_ROOT_DIR;
	protected static String SELENIUM_HOST;
	protected static String SELENIUM_PORT;
	protected static String SELENIUM_REMOTE_URL;
	public static String BROWSER;
	static {
		Props.loadRunConfigProps("/environment.properties");
		DRIVER_ROOT_DIR = Props.getProp("driver.root.dir");

		if (!DRIVER_ROOT_DIR.equals("DEFAULT_PATH")) {
			System.setProperty("webdriver.chrome.driver", getChromeDriverPath());
			System.setProperty("webdriver.gecko.driver", getGekoDriverPath());
		}

		setEnv();
		BROWSER = Props.getSystemProp("browser");
	}

	private static String getDriverPath() {
		DRIVER_PATH = Props.getProp("driver.root.dir");
		return DRIVER_PATH;
	}

	private static String getChromeDriverPath() {
		DRIVER_PATH = Props.getProp("driver.chrome.dir");
		return DRIVER_PATH;
	}

	private static String getGekoDriverPath() {
		DRIVER_PATH = Props.getProp("driver.gecko.dir");
		return DRIVER_PATH;
	}

	public static void setEnv() {
		String env = "";
		env = Props.getSystemProp("env");
		if (env.contains("http")) {
			String pattern = "--(.*?)[.]";
			Pattern p = Pattern.compile(pattern);
			Matcher m = p.matcher(env);
			if (m.find()) {
				String temp = m.group(0).replace("--", "");
				ENV = temp.replace(".", "");
			}
		} else {
			ENV = env;
		}
	}

}
