package test.automation.pageobject;

import org.openqa.selenium.By;
import test.automation.core.PageObject;

public class ContactPage_Actions extends PageObject{
	static By link_submit=By.linkText("Submit");
	static By txt_msg=By.xpath("//*[@id='header-message']/div");
	static By forename=By.id("forename");
	static By surname=By.id("surname");
	static By email=By.id("email");
	static By telephone=By.id("telephone");
	static By message=By.id("message");	
	
	public static void click_submit() {
		click(link_submit);		
	}
	
	public static boolean verify_ErrorMsg() {
		boolean flag=getText(txt_msg).contains("complete the form correctly");
		return flag;
	}
	
	public static void enter_ValidData() {
		enterText(forename, "FirstName");
		enterText(surname, "LastName");
		enterText(email, "Test@TEst.com");
		enterText(telephone, "02 1234 5678");
		enterText(message, "This is test msg");
	}
	
	public static void enter_InvalidData() {
		enterText(forename, "FirstName");
		enterText(surname, "LastName");
		enterText(email, "Test");
		enterText(telephone, "Test");
		enterText(message, "This is test msg");
	}
	 
}
